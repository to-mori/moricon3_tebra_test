
#include <xc.h>
#define _XTAL_FREQ 8000000
#pragma config BOREN = OFF
#pragma config FOSC = INTRC_NOCLKOUT
#pragma config FCMEN = OFF
#pragma config MCLRE = OFF
#pragma config WDTE = OFF
#pragma config LVP = OFF
#pragma config PWRTE = ON

#define timerH cf
#define timerL 2c
#define LockCounter 10                                //lock time counter
#define UnlockCounter 10                              //unlock time counter
#define IntervalCounter 30                            //interval time counter
#define StopCounter 10                                //stop counter

int mode;                                             //mode
    //mode 0:stop
    //mode 1:operating
    //mode 2:irregular stop
int st;                                               //state flag
    //st 1:lock action
    //st 2:lock action -> interval
    //st 3:unlock action
    //st 4:unlock action -> interval
int ast;                                              //auto stop state flag
    //ast 1:last interval
    //ast 2:last unlock action
int lc;                                               //lock time counter
int ic;                                               //interval time counter
int uc;                                               //unlock time counter
int dc;                                               //drive command counter
int mac;                                              //main action counter
int sac;                                              //sabu action counter
int sc;                                               //stop counter
int RB4bfr;                                           //main before
int RB5bfr;                                           //sabu berore


/*main setting*/
void main(void)
{
////input////
//RB0:change sw
//RB1:start sw
//RB2:stop sw
//RB3:stop signal input
//RB4:main input
//RB5:sabu input

////output////
//RA3:unlock count output
//RA4:drive command count output
//RA6:blue LED
//RC0:power LED
//RC1:yellow LED
//RC2:greem LED
//RC3:red LED
//RC4:lock count output
//RC5:driver in2
//RC6:driver in1

OSCCON=0b01110000;                                          //clock 8MHz
CM1CON0=0b00000000;                                         //comparator OFF
PORTA=0b00000000;                                           //portA output 0
PORTB=0b00000000;                                           //portB output 0
PORTC=0b00000000;                                           //portC output 0
TRISA=0b00000000;                                           //portA input
TRISB=0b00111111;                                           //portB input RB0,1,2,3,4,5
TRISC=0b00000000;                                           //portC input -

ANSEL=0b00000000;                                           //nothing analog
ANSELH=0b00000000;                                          //nothing analog

WPUB=0b00111111;                                            //RB0,1,2,3,4,5 internal pull up
OPTION_REG=0b00000000;                                      //pull up enable

/*timer interrupt 1 setting*/
T1CON=0x30;                                                 //interval timer
TMR1H=0xtimerH;                                             //timer H
TMR1L=0xtimerL;                                             //timer L
TMR1IE=1;                                                   //interrupt ok
TMR1ON=1;                                                   //timer1 start

PEIE=1;                                                     //all interrupt ok
GIE=1;                                                      //all interrupt ok

/*default*/
mode=0;                                                     //mode = stop
st=0;                                                       //state = standby
ast=1;                                                      //auto stop state = last interval
lc=0;                                                       //lock time counter = 0
uc=0;                                                       //unlock time counter = 0
ic=0;                                                       //interval time counter = 0
dc=0;                                                       //drive comand counter = 0
mac=0;                                                      //main action counter = 0
sac=0;                                                      //sabu action counter = 0
sc=0;                                                       //stop counter = 0

///////////////////////main loop//////////////////////////
while(1)                                                    //main loop
    {
    RC0=1;                                                  //power LED ON
    }
}
/////////////////////timer interrupt//////////////////////
void interrupt A(void)
{
    if(TMR1IF)
        {
        TMR1H=0xtimerH;                                     //timer H
        TMR1L=0xtimerL;                                     //timer L
        }

////////manual drive///////
if(RB0==0)                                                  //change sw ON
    {
    if(RB1==0)                                              //if start sw ON
        {
        RC4=1;                                              //lock SW ON
        RC2=1;                                              //green LED ON
        lc=0;ic=0;uc=0;dc=0;mac=0;sac=0;sc=0;               //all count reset
        }
    else
        {
        if(RB2==0)                                          //if stop sw ON
            {
            RA3=1;                                          //unlock SW ON
            RA6=1;                                          //blue LED ON
            lc=0;ic=0;uc=0;dc=0;mac=0;sac=0;sc=0;           //all count reset
            }
        else
            {
            RC4=0;RA3=0;                                    //lock,unlock SW OFF
            RC2=0;RA6=0;                                    //green,blue LED OFF
            lc=0;ic=0;uc=0;dc=0;mac=0;sac=0;sc=0;           //all count reset
            }
        }
    }
else
    {

////////auto drive////////
    ////////main internal count////////
    if(RB4==0)                                              //main input ON
        {
        RC1=1;                                              //yellow LED ON
        if(RB4bfr==1 && RB4==0)                             //if main input OFF->ON
            {
            if(mode==1)                                     //if operating
                {
                //mac++;                                      //main time count +1
                }
            }
        }
    else
        {
        RC1=0;                                              //yellow LED OFF
        }

    ////////sabu internal count////////
    if(RB5==0)                                              //sabu input ON
        {
        if(RB5bfr==1 && RB5==0)                             //if sabu input OFF->ON
            {
            if(mode==1)                                     //if operating
                {
                //sac++;                                      //sabu time count +1
                }
            }
        }

    ////////lock,unlock SW drive////////
    switch(mode)                                            //switch(mode)
        {
        ////////case stop///////
        case 0:                                             //case stop
            if(RB1==0)                                      //if start sw ON
                {
                mode=1;                                     //mode = operating
                if(RB4==1)                                  //if main input OFF
                    {
                    st=1;                                   //state = lock action
                    }
                else
                    {
                    st=3;                                   //state = unlock action
                    }
                }
        break;                                              //case stop break
        //////case operating//////
        case 1:                                             //case operating
            if(dc-mac==0 && dc-sac==0)                      //if count no problem
                {
                if(RB3==1)                                  //if stop signal OFF
                    {
                    if(RB2==1)                              //if stop sw OFF
                        {
                        switch(st)                          //switch(state)
                            {
                            case 1:                         //if lock action
                                if(lc>=LockCounter)         //if lock time count >= 500ms
                                    {
                                    RC4=0;RA3=0;            //lock,unlock SW OFF
                                    RC2=0;                  //green LED OFF
                                    RA4=0;                  //drive count output OFF
                                    lc=0;                   //lock time count reset
                                    dc++;                   //drive comand count +
                                    if(RB4==0)              //if main input ON
                                        {
                                        mac++;              //main count ++
                                        }
                                    if(RB5==0)              //if sabu input ON
                                        {
                                        sac++;              //sabu count ++
                                        }
                                    st=2;                   //state = lock action -> interval
                                    }
                                else
                                    {
                                    RC4=1;RA3=0;            //lock SW ON
                                    RC2=1;                  //green LED ON
                                    RA4=0;                  //drive count output OFF
                                    lc++;                   //lock time count +1
                                    if(lc==1)               //if first count
                                        {
                                        RA4=1;             //drive count output ON
                                        }
                                    else
                                        {
                                        RA4=0;             //drive count output OFF
                                        }
                                    }
                            break;                          //case 1 break
                            case 2:                         //if normal -> interval
                                if(ic>=IntervalCounter)     //if interval time count >=2000ms
                                    {
                                    RC4=0;RA3=0;            //lock,unlock SW OFF
                                    RA4=0;                  //drive count output OFF
                                    ic=0;                   //interval time count reset
                                    st=3;                   //state = reversal drive
                                    }
                                else
                                    {
                                    RC4=0;RA3=0;            //lock,unlock SW OFF
                                    RA4=0;                  //drive count output OFF
                                    ic++;                   //interval time count +1
                                    }
                            break;                          //case 2 break
                            case 3:                         //if reversal
                                if(uc>=UnlockCounter)       //if unlock time count >=500ms
                                    {
                                    RC4=0;RA3=0;            //lock,unlock SW OFF
                                    RA6=0;                  //blue LED OFF
                                    RA4=0;                  //drive count output OFF
                                    uc=0;                   //unlock time count reset
                                    dc++;                   //drive comand count +
                                    if(RB4==1)              //if main input OFF
                                        {
                                        mac++;              //main count ++
                                        }
                                    if(RB5==1)              //if sabu input OFF
                                        {
                                        sac++;              //sabu count ++
                                        }
                                    st=4;                   //state = unlock -> interval
                                    }
                                else
                                    {
                                    RC4=0;RA3=1;            //unlock SW ON
                                    RA6=1;                  //blue LED ON
                                    RA4=0;                  //drive count output OFF
                                    uc++;                   //unlock time count +1
                                    }
                            break;                          //case 2 break
                            case 4:                         //if lock -> interval
                                if(ic>=IntervalCounter)     //if interval time count >=2000ms
                                    {
                                    RC4=0;RA3=0;            //lock,unlock SW OFF
                                    RA4=0;                  //drive count output OFF
                                    ic=0;                   //interval time count reset
                                    st=1;                   //state = normal drive
                                    }
                                else
                                    {
                                    RC4=0;RA3=0;            //lock,unlock SW OFF
                                    RA4=0;                  //drive count output OFF
                                    ic++;                   //interval time count +1
                                    }
                            break;                          //case 2 break
                        default:
                            break;                          //st other break
                            }
                    }

        //////manual stop//////
                else
                    {
                    RC4=0;RA3=0;                            //lock,unlock SW OFF
                    RA6=0;RC2=0;                            //blue,green LED
                    RA4=0;                                  //drive count output OFF
                    mode=0;                                 //mode = stop
                    lc=0;ic=0;uc=0;dc=0;mac=0;sac=0;sc=0;   //all count reset
                    st=0;                                   //state = standby
                    }
                }

        //////auto stop//////
            else
                {
                if(ast==1)                                          //switch(auto stop state)
                    {                                               //if last lock action
                    if(ic>=IntervalCounter)                         //if interval time count >=2000ms
                           {
                            RC4=0;RA3=0;                            //lock,unlock SW OFF
                            RA6=0;RC2=0;                            //blue,green LED OFF
                            RA4=0;                                  //drive count output OFF
                            ic=0;                                   //interval time count reset
                            ast=2;                                  //auto stop state = last unlock action
                           }
                       else
                           {
                            RC4=0;RA3=0;                            //lock,unlock SW OFF
                            RA6=0;RC2=0;                            //blue,green LED OFF
                            RA4=0;                                  //drive count output OFF
                            ic++;                                   //interval time count +1
                           }
                    }
                else
                    {
                    if(uc>=UnlockCounter)                           //if unlock time count >=500ms
                           {
                            RC4=0;RA3=0;                            //lock,unlock SW OFF
                            RA6=0;                                  //blue LED OFF
                            RA4=0;                                  //drive count output OFF
                            uc=0;                                   //unlock time count reset
                            lc=0;ic=0;uc=0;dc=0;mac=0;sac=0;sc=0;   //all count reset
                            ast=1;                                  //auto stop state = nothing
                            mode=0;                                 //mode = stop
                           }
                       else
                           {
                            RC4=0;RA3=1;                            //unlock SW ON
                            RA6=1;                                  //blue LED ON
                            RA4=0;                                  //drive count output OFF
                            uc++;                                   //unlock time count +1
                           }
                    }
                }
            }

        //////ireegular stop//////
            else
                {
                RC4=0;RA3=0;                                //lock,unlock SW OFF
                RA6=0;RC2=0;                                //blue,green LED OFF
                RC3=1;                                      //red LED ON
                RA4=0;                                      //drive count output OFF
                mode=2;                                     //mode = irregular stop
                lc=0;ic=0;uc=0;dc=0;mac=0;sac=0;sc=0;       //all count reset
                st=0;                                       //state = standby
                }
            break;                                          //case operating break

        //////case irregular stop//////
        case 2:                                         //case irregular stop
            RC4=0;RA3=0;                                //lock,unlock SW OFF
            RA6=0;RC2=0;                                //blue,green LED
            RC3=1;                                      //red LED ON
            RA4=0;                                      //drive count output OFF
            mode=2;                                     //mode = irregular stop
            lc=0;ic=0;uc=0;dc=0;mac=0;sac=0;sc=0;       //all count reset
            st=0;
        break;                                          //case irregular stop break

        default:
        break;
        }
    }
RB4bfr=RB4;                                             //RB4bfr -> renew
RB5bfr=RB5;                                             //RB5bfr -> renew
TMR1IF=0;                                               //timer1 flag clear
}
